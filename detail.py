import requests, json, sys, os, urllib.parse, time, random, re

if hasattr(sys, "getwindowsversion"):
  import win_unicode_console
  win_unicode_console.enable()

classes = {}
processed = {}
processed_csv = {}

results = open("results.json", "a+", encoding="utf-8")
csv = open("results.csv", "a+", encoding="utf-8")

def get_file_data(file_name):
  global classes

  f = open(file_name, encoding="utf-8")
  for i in f.readlines():
    data = i.replace("\"", "").replace("\n", "");
    splited = data.split(";")

    if len(splited) < 4:
      continue

    if file_name == "results.csv":
      processed_csv[splited[0]] = True
      continue
    
    if not splited[3].lower() in ["s", "y"]:
      continue

    if not splited[1] in processed:
      classes[splited[1]] = splited[0]
  f.close()

def write_csv(result_js):
  js = json.loads(result_js)
 
  if js["app"] in  processed_csv:
     print("Registred key",js["app"])
     return
  app = "\"{}\"".format(js["app"])
  title = "\"{}\"".format(js["data"][0][0][0])
  link = "\"{}\"".format("https://play.google.com/store/apps/details?id="+js["app"])
  description = "\"{}\"".format(js["data"][0][10][0][1])
  csv.write("{};{};{};{}\n".format(app,title, link, description))



def process_data(argv):
  q = urllib.parse.quote(argv)

  functions_dic = {}


  headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Cache-Control': 'max-age=0',
  }

  params = (
    ('id', q),
  )
 
  r = requests.get('https://play.google.com/store/apps/details', headers=headers, params=params)
  fhtml=open("detail.html", "w+", encoding="utf-8")
  fhtml.write(r.text)
  fhtml.close() 
  str_text = r.text
  last_pos = 1
  actual_pos = 0
  while   last_pos > 0:
    actual_pos = last_pos
    last_pos = str_text.find("{key: 'ds:", last_pos + 1)
    if last_pos > 0:
      end_pos = str_text.find("});", last_pos)
    else:
      continue;
    
    js = str_text[last_pos:end_pos+1].replace("'", "\"", 4)
    js = js.replace("key:", "\"key\":", 1).replace("isError:", "\"isError\":").replace("hash:", "\"hash\":").replace("data:", "\"data\":").replace("sideChannel:", "\"sideChannel\":")
    func_data = json.loads(js)
    functions_dic[func_data["key"]] = func_data

  if not "ds:5" in functions_dic:
    print("Invalid app response data", q)
    return

  result_js = "{\"app\":\""+argv+"\", \"data\":"+json.dumps(functions_dic["ds:5"]["data"])+"}"
  results.write(result_js+",\n")
  write_csv(result_js)


  #results.close()




if os.path.exists("results.json"):
   f = open("results.json", encoding="utf-8")
   for i in f.readlines():
     js = json.loads(i[:-2])
     processed[js["app"]] = True
   f.close()
   


for i in os.listdir("."):
  if i.endswith("csv"):
    get_file_data(i)


#Write json registred apps in cvs
f = open("results.json", encoding="utf-8")

for i in f.readlines():
  write_csv(i[:-2])

f.close()

print("{} classes will be imported".format(len(classes)))

for i in classes:
  interval = random.randint(1,4)
  print("Getting information form ", i, "interval: ", interval)
  process_data(i)
  time.sleep(interval)


results.close()


csv.close()
