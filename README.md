# Play Store Search

A tool developed in python 3 to search for applications on the Google Play Store to build a database for support scientific research


## How to use it


```sh
python3 mycurl.py  "some search expression"
```

Two files will be generate using que search expresssion like:
```sh
some_search_expression.csv
some_search_expression.json
```

They files contains the search result.
