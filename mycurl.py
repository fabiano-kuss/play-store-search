import requests, json, sys, os, urllib.parse, time, datetime

if hasattr(sys, "getwindowsversion"):
  import win_unicode_console
  win_unicode_console.enable()


imported = {}

q = urllib.parse.quote(sys.argv[1])
dt = datetime.datetime.now()
jar = "{}-{}-{}-{}".format(dt.year, dt.month, dt.day, dt.hour) 
next_b64 = ""


if not os.path.exists("result0.json"):
  url = "https://play.google.com/store/search?q="+q+"&c=apps"

  #print(url)
  cookies = {
    '_ga': 'GA1.3.1630800735.1603202626',
    '_gid': 'GA1.3.455530190.1603202626',
    'OTZ': '5681644_68_64_73560_68_416340',
    '1P_JAR': jar
  }


  headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
  }

  params = (
    ('q', q),
    ('c', 'apps'),
  )

  r = requests.get(url, headers=headers, cookies=cookies)

  f = open("result0.json", "w", encoding="utf-8")
  f.write(r.text)
  f.close()

  f = open("result0.json", encoding="utf-8")
  
  str_text = ""

  for i in f.readlines():
    str_text += i;

  last_pos = 0
  last_pos = str_text.find("{key: 'ds:3", last_pos + 1)
  last_pos = str_text.find("data:", last_pos + 1)+5
  if last_pos > 0:
    end_pos = str_text.find(", sideChannel: {}}", last_pos)-3
  list_text = str_text[last_pos:end_pos + 3].replace("null", "\"null\"").replace("true", "True").replace("false", "False")
  data_list = eval(list_text)
  result_next = data_list[0][1][0][0][7]
  data_list = data_list[0][1][0][0][0]
  for i in data_list:
    imported[i[12][0]] = i[2]

  f.close() 
  os.remove("result0.json")

  if len(result_next) < 1:
    print("Less tha 50 records found")
    next_b64 = ""
  else:
    next_b64 = result_next[1]

else:
  f = open("result0.json", encoding="utf-8")
  str_text = ""
  for i in f.readlines():
    str_text += i;

  f.close()

  last_pos = 0
  last_pos = str_text.find("{key: 'ds:3", last_pos + 1)
  last_pos = str_text.find("data:", last_pos + 1)+5
  if last_pos > 0:
    end_pos = str_text.find(", sideChannel: {}}", last_pos)-3
  list_text = str_text[last_pos:end_pos + 3].replace("null", "\"null\"").replace("true", "True").replace("false", "False")
  #print(list_text)
  data_list = eval(list_text)
  result_next = data_list[0][1][0][0][7]
  data_list = data_list[0][1][0][0][0]
  for i in data_list:
    imported[i[12][0]] = i[2]
    #print(i[2], i[12][0])
  
  os.remove("result0.json")
  if len(result_next) < 1:
    print("Less tha 50 records found")
    next_b64 = ""
  else:
    next_b64 = result_next[1]

  print(next_b64);
#################################################################batchexecute#######################################################################################################

cnt = 1

for i  in range(0,4):
  if next_b64 == "":
    print("No more records")
    break

  time.sleep(5)

  print("Reading file: "+"result"+str(cnt)+".json")

  if not os.path.exists("result"+str(cnt)+".json"):

    cookies = {
    'NID': '204=hgWHx5smc1j91F1rCHs_Vf8cZqGnWkOrUxTFZDB_4zYrBpFzFWcFzv5mgWGSU4Prvgx3obeJMS51uHsYwr-e6u-XX-yl_MdMQVxNS3vXUAaE8mGxwU_qeV0zhahQMcnRtlOZT5_qG9NPhyYNLTJPIQ4KXoJTGtx30f_Njr4hYqA',
    '_ga': 'GA1.3.1630800735.1603202626',
    '_gid': 'GA1.3.455530190.1603202626',
    'OTZ': '5681644_68_64_73560_68_416340',
    '1P_JAR': jar,
    }

    headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0',
    'Accept': '*/*',
    'Accept-Language': 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
    'Referer': 'https://play.google.com/',
    'X-Same-Domain': '1',
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    'Origin': 'https://play.google.com',
    'DNT': '1',
    'Connection': 'keep-alive',
    'TE': 'Trailers',
    }

    params = (
    ('rpcids', 'qnKhOb'),
      ('f.sid', '-7071023086272843998'),
      ('hl', 'pt-BR'),
      ('authuser', ''),
      ('soc-app', '121'),
      ('soc-platform', '1'),
      ('soc-device', '1'),
      ('_reqid', '1541092'),
      ('rt', 'c'),
    )

    data = {
  'f.req': '[[["qnKhOb","[[null,[[10,[10,50]],true,null,[96,27,4,8,57,30,110,11,16,49,1,3,9,12,104,55,56,51,10,34,31,77,145],[null,null,null,[[[[7,31],[[1,52,43,112,92,58,69,31,19,96,103]]]]]]],null,\\"'+next_b64+'\\"]]",null,"generic"]]]',
  'at': 'AE2DSOBziEC9n001Rh5-go31Tb3T:1603151289194',
  '': ''
    }

    r = requests.post('https://play.google.com/_/PlayStoreUi/data/batchexecute', headers=headers, params=params, data=data, cookies=cookies)

    f = open("result"+str(cnt)+".tmp", "w", encoding="utf-8")
    f.write(r.text)
    f.close()

    f = open("result"+str(cnt)+".tmp", encoding="utf-8")
    f.readline();
    f.readline();
    num = int(f.readline())
    js = f.readline()[:-1] + "]"
    js = "{\"data\":"+ js +"}"
    data = json.loads(js)
    value = eval(data["data"][0][2].replace("null", "\"null\"").replace("true", "True").replace("false", "False"))
    fdata = open("result"+str(cnt)+".json", "w", encoding="utf-8")
    fdata.write(json.dumps(data))
    fdata.close();
    f.close()
    os.remove("result"+str(cnt)+".tmp") 
    os.remove("result"+str(cnt)+".json") 

  else:

    with open("result"+str(cnt)+".json", encoding="utf-8") as f:
      data = json.load(f)
    value = eval(data["data"][0][2].replace("null", "\"null\"").replace("true", "True").replace("false", "False"))

  data_list = value[0][0][0]
  
  for i in data_list:
    imported[i[12][0]] = i[2]

  if len(value[0][0][7]) > 0:
    if value[0][0][7] == "null":
       next_b64 = ""
    else:
      next_b64 = value[0][0][7][1]
  else:
    next_b64 = ""

  cnt += 1

f = open(q+".csv", "w", encoding="utf-8")
fjs = open(q+".json", "w", encoding="utf-8")
for i in imported:
  f.write("\""+imported[i]+"\";"+"\""+i+"\";\"https://play.google.com/store/apps/details?id="+i+"\"\n")

fjs.write(json.dumps(imported))

f.close()
fjs.close()
